/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Utiles {

//    public static String encriptPass(String pass) {
//        byte[] newPassword = null;
//        try {
//            newPassword = MessageDigest.getInstance("SHA-1").digest(pass.getBytes("UTF-8"));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        String encriptado = Base64.getEncoder().encodeToString(newPassword);
//        return encriptado;
//    }
    
    public static String comprobarPassword(String pass, String pass2) throws Exception {
        if (pass.equals(pass2)) {
            return Utiles.encriptPass(pass2);
        } else {
            throw new Error("Las contraseñas no coinciden");
        }
    }
    
    public static String encriptPass(String x) throws Exception {
        String password = x;
        String algorithm = "SHA";

        byte[] plainText = password.getBytes();

        java.security.MessageDigest md = java.security.MessageDigest.getInstance(algorithm);

        md.reset();
        md.update(plainText);
        byte[] encodedPassword = md.digest();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < encodedPassword.length; i++) {
            if ((encodedPassword[i] & 0xff) < 0x10) {
                sb.append("0");
            }
            sb.append(Long.toString(encodedPassword[i] & 0xff, 16));
        }
        return sb.toString();
    }


}
