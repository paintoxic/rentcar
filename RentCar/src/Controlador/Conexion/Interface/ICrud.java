/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Conexion.Interface;

import java.util.List;

/**
 *
 * @author heze-
 * @param <T>
 */
public interface ICrud<T> {
    public boolean create(T item);
    public boolean delete(T item);
    public boolean update(T item);
    public T read(Object key);
    public List<T> readAll(T filter);
}
