/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Conexion.Facade;

import Controlador.DAO.PersonaDAO;
import Controlador.DTO.PersonaDTO;

/**
 *
 * @author heze-
 */
public class PersonaFacade {

    public PersonaFacade() {
    }
    
    public PersonaDTO traerPersona(Long documento){
        PersonaDAO perDAO = new PersonaDAO();
        return perDAO.read(documento);
    }
    
    public boolean updatePassword(PersonaDTO persona){
        PersonaDAO perDAO = new PersonaDAO();
        return perDAO.updatePass(persona);
    }
    
    public boolean updatePersona(PersonaDTO persona){
        PersonaDAO perDAO = new PersonaDAO();
        return perDAO.update(persona);
    }
    
}
