/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Conexion.Facade;

import Controlador.DAO.SucursalDAO;
import Controlador.DTO.SucursalDTO;
import java.util.List;

/**
 *
 * @author heze-
 */
public class SucursalFacade {

    public SucursalFacade() {
    }
    
    public boolean crearSucursal(String nombre,String direccion,int id_ciudad){
        SucursalDAO sucDAO = new SucursalDAO();
        SucursalDTO sucDTO = new SucursalDTO();
        sucDTO.setNombreSucursal(nombre);
        sucDTO.setDireccion(direccion);
        sucDTO.setId_ciudad(id_ciudad);
        return sucDAO.create(sucDTO);
    }
    
    public List<SucursalDTO> traerSucursales(SucursalDTO filter){
        SucursalDAO sucDAO = new SucursalDAO();
        return sucDAO.readAll(filter);
    }
    
    
}
