/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.ViewAutosDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class ViewAutosDAO implements ICrud<ViewAutosDTO> {

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_READ_ONE = "SELECT * FROM lista_autos WHERE placa = ?;";
    private static final String SQL_READ = "SELECT * FROM lista_autos;";

    @Override
    public boolean create(ViewAutosDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(ViewAutosDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(ViewAutosDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ViewAutosDTO read(Object key) {
        ViewAutosDTO objRes = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ_ONE);
            psnt.setString(1, String.valueOf(key));
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                objRes = new ViewAutosDTO(rs.getString("placa"),
                            rs.getInt("modelo"),
                            rs.getString("estado"),
                            rs.getFloat("precio_dia"),
                            rs.getString("marca"),
                            rs.getString("color"),
                            rs.getString("sucursal"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return objRes;
    }

    @Override
    public List<ViewAutosDTO> readAll(ViewAutosDTO filter) {
        List<ViewAutosDTO> listRes = new ArrayList<>();
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ);
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                ViewAutosDTO objRes = new ViewAutosDTO(rs.getString("placa"),
                            rs.getInt("modelo"),
                            rs.getString("estado"),
                            rs.getFloat("precio_dia"),
                            rs.getString("marca"),
                            rs.getString("color"),
                            rs.getString("sucursal"));
                listRes.add(objRes);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return listRes;
    }

}
