/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.CrearClienteDTO;
import Controlador.DTO.InicioSesionDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class InicioSesionDAO {

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_READ = "SELECT * FROM inicio_sesion WHERE usuario = ? AND password = ?;";
       
    
    public InicioSesionDTO read(InicioSesionDTO key) {
        InicioSesionDTO objRes = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ);
            psnt.setString(1, key.getUsuario());
            psnt.setString(2, key.getPassword());
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                objRes = new InicioSesionDTO(rs.getLong("documento"),
                                    rs.getString("usuario"),
                                    rs.getString("password"),
                                    rs.getString("perfil"));
            }
            if(objRes == null){
                throw new Error("Usuario o contraseña incorrectos");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return objRes;
    }
    
}
