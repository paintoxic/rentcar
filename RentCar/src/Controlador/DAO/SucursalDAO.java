/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.SucursalDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class SucursalDAO implements ICrud<SucursalDTO>{

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_INSERT = "INSERT INTO sucursal (nombre,direccion,id_ciudad) VALUES(?,?,?)";
    private static final String SQL_DELETE = "DELETE FROM sucursal WHERE id_sucursal = ?;";
    private static final String SQL_UPDATE = "UPDATE sucursal SET nombre = ? WHERE id_sucursal = ?;";
    private static final String SQL_READ = "SELECT * FROM sucursal WHERE id_sucursal = ?;";
    private static final String SQL_READ_ALL = "SELECT * FROM sucursal;";
            
    @Override
    public boolean create(SucursalDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setString(1, item.getNombreSucursal());
            ps.setString(2, item.getDireccion());
            ps.setInt(3, item.getId_ciudad());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(SucursalDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setLong(1, item.getId_sucursal());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean update(SucursalDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setString(1, item.getNombreSucursal());
            ps.setInt(2, item.getId_sucursal());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public SucursalDTO read(Object key) {
        SucursalDTO objRes = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ);
            psnt.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                objRes = new SucursalDTO(rs.getInt("id_sucursal"),
                                    rs.getString("nombre"),
                                    rs.getString("direccion"),
                                    rs.getInt("id_ciudad"));                
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return objRes;
    }

    @Override
    public List<SucursalDTO> readAll(SucursalDTO filter) {
        List<SucursalDTO> lst = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ_ALL);
            ResultSet rs = psnt.executeQuery();
            lst = new ArrayList<SucursalDTO>();
            while (rs.next()) {
                SucursalDTO obj = new SucursalDTO(rs.getInt("id_sucursal"),
                                    rs.getString("nombre"),
                                    rs.getString("direccion"),
                                    rs.getInt("id_ciudad"));
                lst.add(obj);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return lst;
    }
    
    
    
}
