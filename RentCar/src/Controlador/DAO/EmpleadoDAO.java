/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.EmpleadoDTO;
import Controlador.DTO.PersonaDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class EmpleadoDAO implements ICrud<EmpleadoDTO>{

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_INSERT = "INSERT INTO crear_empleados VALUES(?,?,?,?,?,?,?,?);";
    private static final String SQL_READ = "SELECT * FROM empleado WHERE id_persona = ?";
    
    @Override
    public boolean create(EmpleadoDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(EmpleadoDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(EmpleadoDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EmpleadoDTO read(Object key) {
        EmpleadoDTO objRes = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ);
            psnt.setLong(1, Long.valueOf(String.valueOf(key)));
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                objRes = new EmpleadoDTO(rs.getInt("id_empleado"),
                                    rs.getLong("id_persona"),
                                    rs.getInt("id_sucursal"),
                                    rs.getInt("id_cargo"),
                                    rs.getInt("comisiones"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return objRes;
    }

    @Override
    public List<EmpleadoDTO> readAll(EmpleadoDTO filter) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean createEmpleado(EmpleadoDTO emp,PersonaDTO persona) {
        PreparedStatement ps;
        SucursalDAO sucDao = new SucursalDAO();
        CargoDAO carDAO = new CargoDAO();
        try {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setLong(1, persona.getDocumento());
            ps.setString(2, persona.getNombre());
            ps.setString(3, persona.getDireccion());
            ps.setString(4, persona.getTelefono());
            ps.setString(5, persona.getUsuario());
            ps.setString(6, persona.getPassword());
            ps.setString(7, sucDao.read(emp.getId_sucursal()).getNombreSucursal());
            ps.setString(8, carDAO.read(emp.getId_cargo()).getNombre());            
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        }catch(Exception e){
            throw new Error(e.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }
    
}
