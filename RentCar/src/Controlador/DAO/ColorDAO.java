/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.CargoDTO;
import Controlador.DTO.ColorDTO;
import Controlador.DTO.SucursalDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class ColorDAO implements ICrud<ColorDTO>{

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_INSERT = "INSERT INTO color (nombre) VALUES(?)";
    private static final String SQL_DELETE = "DELETE FROM color WHERE id_color = ?;";
    private static final String SQL_UPDATE = "UPDATE cargo SET nombre = ? WHERE id_color = ?;";
    private static final String SQL_READ = "SELECT * FROM cargo WHERE id_color = ?;";
    private static final String SQL_READ_ALL = "SELECT * FROM color;";
            
    @Override
    public boolean create(ColorDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setString(1, item.getNombre());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(ColorDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setLong(1, item.getId_color());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean update(ColorDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setString(1, item.getNombre());
            ps.setInt(2, item.getId_color());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public ColorDTO read(Object key) {
        ColorDTO objRes = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ);
            psnt.setInt(1, Integer.parseInt(String.valueOf(key)));
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                objRes = new ColorDTO(rs.getInt("id_color"),
                                    rs.getString("nombre"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return objRes;
    }

    @Override
    public List<ColorDTO> readAll(ColorDTO filter) {
        List<ColorDTO> lst = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ_ALL);
            ResultSet rs = psnt.executeQuery();
            lst = new ArrayList<ColorDTO>();
            while (rs.next()) {
                ColorDTO obj = new ColorDTO(rs.getInt("id_color"),
                                    rs.getString("nombre"));
                lst.add(obj);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return lst;
    }
    
    
    
}
