/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.AlquilerAutoDTO;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class AlquilerAutoDAO implements ICrud<AlquilerAutoDTO> {

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_READ = "SELECT * from alquiler_auto WHERE id_alquiler = ?;";
    private static final String SQL_READ_ONE = "SELECT * from alquiler_auto WHERE id_alquilerauto = ?;";
    private static final String SQL_UPDATE = "UPDATE alquiler_auto SET fechainicio = ? ,fechafin = ?, placa = ?, galonesentregado = ? where id_alquilerauto = ?;";
    private static final String SQL_DELETE = "DELETE FROM alquiler_auto WHERE id_alquilerauto = ?";
    private static final String SQL_INSERT = "INSERT INTO alquiler_auto (id_alquiler,placa,fechainicio,fechafin,galonesentregado) VALUES (?,?,?,?,?)";

    @Override
    public boolean create(AlquilerAutoDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setInt(1, item.getId_alquiler());
            ps.setString(2, item.getPlaca());
            ps.setDate(3, item.getFechaInicio());
            ps.setDate(4, item.getFechaFin());
            ps.setInt(5, item.getGalonesentregado());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(AlquilerAutoDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setInt(1, item.getId_alquiler_auto());           
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean update(AlquilerAutoDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setDate(1, item.getFechaInicio());
            ps.setDate(2, item.getFechaFin());
            ps.setString(3, item.getPlaca());
            ps.setInt(4, item.getGalonesentregado());
            ps.setInt(5, item.getId_alquiler_auto());            
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public AlquilerAutoDTO read(Object key) {
        AlquilerAutoDTO objRes = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ_ONE);
            psnt.setInt(1, Integer.parseInt(String.valueOf(key)));
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                objRes = new AlquilerAutoDTO(rs.getInt("id_alquilerauto"),
                        rs.getInt("id_alquiler"),
                        rs.getString("placa"),
                        rs.getDate("fechainicio"),
                        rs.getDate("fechafin"),
                        rs.getInt("galonesentregado"),
                        rs.getFloat("precio_alquiler_auto"));                
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return objRes;
    }

    @Override
    public List<AlquilerAutoDTO> readAll(AlquilerAutoDTO filter) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<AlquilerAutoDTO> findByIdAlquiler(int idAlquiler) {
        List<AlquilerAutoDTO> listRes = new ArrayList<AlquilerAutoDTO>();
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ);
            psnt.setInt(1, idAlquiler);
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                AlquilerAutoDTO objRes = new AlquilerAutoDTO(rs.getInt("id_alquilerauto"),
                        rs.getInt("id_alquiler"),
                        rs.getString("placa"),
                        rs.getDate("fechainicio"),
                        rs.getDate("fechafin"),
                        rs.getInt("galonesentregado"),
                        rs.getFloat("precio_alquiler_auto"));
                listRes.add(objRes);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return listRes;
    }
}
