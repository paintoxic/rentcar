/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.CiudadDTO;
import Controlador.DTO.CrearClienteDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class CiudadDAO implements ICrud<CiudadDTO>{

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_INSERT = "INSERT INTO ciudad (nombre) VALUES (?);";    
    private static final String SQL_READ = "SELECT * FROM CIUDAD;";
    private static final String SQL_UPDATE = "UPDATE ciudad SET nombre = ? WHERE id_ciudad = ?;";
    private static final String SQL_DELETE = "DELETE FROM ciudad WHERE id_ciudad = ?;";
    
    @Override
    public boolean create(CiudadDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setString(1, item.getNombre());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(CiudadDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setInt(1, item.getId_ciudad());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean update(CiudadDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setString(1, item.getNombre());
            ps.setInt(2, item.getId_ciudad());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public CiudadDTO read(Object key) {
        return null;
    }

    @Override
    public List<CiudadDTO> readAll(CiudadDTO filter) {
        List<CiudadDTO> lst = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ);
            ResultSet rs = psnt.executeQuery();
            lst = new ArrayList<>();
            while (rs.next()) {
                CiudadDTO obj = new CiudadDTO(rs.getInt("id_ciudad"),
                                    rs.getString("nombre"));
                lst.add(obj);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return lst;
    }
 
           
    
}
