/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.PersonaDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class PersonaDAO implements ICrud<PersonaDTO> {

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_INSERT = "INSERT INTO persona (documento,nombre,direccion,telefono,usuario,password) VALUES(?,?,?,?,?,?);";
    private static final String SQL_UPDATE_PRINCIPAL_DATA = "UPDATE persona SET nombre=?,direccion=?,telefono=?,usuario=?  where documento=?;";
    private static final String SQL_UPDATE_PASSWORD = "UPDATE persona SET password = ?  where documento=?;";
    private static final String SQL_DELETE = "DELETE FROM persona WHERE documento = ?;";
    private static final String SQL_READ = "SELECT * FROM persona WHERE documento = ?;";
    private static final String SQL_READALL = "SELECT * FROM persona;";

    @Override
    public boolean create(PersonaDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setLong(1, item.getDocumento());
            ps.setString(2, item.getNombre());
            ps.setString(3, item.getDireccion());
            ps.setString(4, item.getTelefono());
            ps.setString(5, item.getUsuario());
            ps.setString(6, item.getPassword());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(PersonaDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setLong(1, item.getDocumento());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean update(PersonaDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_UPDATE_PRINCIPAL_DATA);
            ps.setString(1, item.getNombre());
            ps.setString(2, item.getDireccion());
            ps.setString(3, item.getTelefono());
            ps.setString(4, item.getUsuario());
            ps.setLong(5, item.getDocumento());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public PersonaDTO read(Object key) {
        PersonaDTO objRes = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ);
            psnt.setLong(1, Long.valueOf(String.valueOf(key)));
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                objRes = new PersonaDTO(rs.getLong("documento"),
                                    rs.getString("nombre"),
                                    rs.getString("direccion"),
                                    rs.getString("telefono"),
                                    rs.getString("usuario"),
                                    rs.getString("password"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return objRes;
    }

    @Override
    public List<PersonaDTO> readAll(PersonaDTO filter) {
        List<PersonaDTO> lst = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READALL);
            ResultSet rs = psnt.executeQuery();
            lst = new ArrayList<>();
            while (rs.next()) {
                PersonaDTO obj = new PersonaDTO(rs.getLong("documento"),
                                    rs.getString("nombre"),
                                    rs.getString("direccion"),
                                    rs.getString("telefono"),
                                    rs.getString("usuario"),
                                    rs.getString("password"));
                lst.add(obj);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return lst;
    }
    
    
    public boolean updatePass(PersonaDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_UPDATE_PASSWORD);            
            ps.setString(1, item.getPassword());
            ps.setLong(2, item.getDocumento());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

}
