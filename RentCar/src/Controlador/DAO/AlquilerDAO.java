/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.AlquilerDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class AlquilerDAO implements ICrud<AlquilerDTO> {

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_INSERT = "INSERT INTO alquiler (id_alquiler,id_cliente,id_empleado,precio_alquiler) VALUES (?,?,?,?)";
    private static final String SQL_MAXID = "SELECT MAX(id_alquiler) as id FROM alquiler;";
    
    @Override
    public boolean create(AlquilerDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setInt(1, item.getId_alquiler());
            ps.setInt(2, item.getId_cliente());
            ps.setInt(3, item.getId_empleado());
            ps.setFloat(4, item.getPrecio_alquiler());            
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(AlquilerDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(AlquilerDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AlquilerDTO read(Object key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AlquilerDTO> readAll(AlquilerDTO filter) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int maxID() { 
        int maxID = 0;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_MAXID);
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                maxID = rs.getInt("id");
                maxID++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return maxID;
    }
    
}
