/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.AlquilerClienteDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class AlquilerClienteDAO implements ICrud<AlquilerClienteDTO> {

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_READ_CLIENTE = "SELECT * FROM info_alquileres WHERE doc_cliente = ?;";
    private static final String SQL_READONE = "SELECT * FROM info_alquileres WHERE id_alquiler = ?;";
    private static final String SQL_READ_EMPLEADO = "SELECT * FROM info_alquileres WHERE doc_empleado = ?;";
    
    @Override
    public boolean create(AlquilerClienteDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(AlquilerClienteDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(AlquilerClienteDTO item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AlquilerClienteDTO read(Object key) {
        AlquilerClienteDTO listRes = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READONE);
            psnt.setInt(1, Integer.parseInt(String.valueOf(key)));
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {                
                listRes = new AlquilerClienteDTO(rs.getInt("id_alquiler"),
                                    rs.getString("cliente"),
                                    rs.getLong("doc_cliente"),
                                    rs.getString("empleado"),
                                    rs.getLong("doc_empleado"),
                                    rs.getFloat("precio_alquiler"));                
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return listRes;
    }

    @Override
    public List<AlquilerClienteDTO> readAll(AlquilerClienteDTO filter) {
        List<AlquilerClienteDTO> listRes = new ArrayList<AlquilerClienteDTO>();
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ_CLIENTE);
            psnt.setLong(1, filter.getDoc_cliente());
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {                
                AlquilerClienteDTO objRes = new AlquilerClienteDTO(rs.getInt("id_alquiler"),
                                    rs.getString("cliente"),
                                    rs.getLong("doc_cliente"),
                                    rs.getString("empleado"),
                                    rs.getLong("doc_empleado"),
                                    rs.getFloat("precio_alquiler"));
                listRes.add(objRes);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return listRes;
    }
    
    public List<AlquilerClienteDTO> readAllEmpleado(AlquilerClienteDTO filter) {
        List<AlquilerClienteDTO> listRes = new ArrayList<AlquilerClienteDTO>();
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ_EMPLEADO);
            psnt.setLong(1, filter.getDoc_empleado());
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {                
                AlquilerClienteDTO objRes = new AlquilerClienteDTO(rs.getInt("id_alquiler"),
                                    rs.getString("cliente"),
                                    rs.getLong("doc_cliente"),
                                    rs.getString("empleado"),
                                    rs.getLong("doc_empleado"),
                                    rs.getFloat("precio_alquiler"));
                listRes.add(objRes);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return listRes;
    }
    
}
