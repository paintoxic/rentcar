/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DAO;

import Controlador.Conexion.Conexion;
import Controlador.Conexion.Interface.ICrud;
import Controlador.DTO.CrearClienteDTO;
import Controlador.DTO.PersonaDTO;
import Utils.Utiles;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heze-
 */
public class CrearClienteDAO implements ICrud<CrearClienteDTO>{

    private static final Conexion con = Conexion.getInstance();
    private static final String SQL_INSERT = "INSERT INTO crear_cliente VALUES (?,?,?,?,?,?,?,?,?);";
    private static final String SQL_UPDATE = "UPDATE crear_cliente SET referido = ? where documento = ?;";
    private static final String SQL_READ = "SELECT * FROM crear_cliente WHERE documento = ?";
    private static final String SQL_READALL = "SELECT * FROM crear_cliente";
    
    @Override
    public boolean create(CrearClienteDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setLong(1, item.getDocumento());
            ps.setString(2, item.getNombre());
            ps.setString(3, item.getDireccion());
            ps.setString(4, item.getCiudad());
            ps.setString(5, item.getTelefono());
            ps.setString(6, item.getUsuario());
            ps.setString(7, item.getPassword());
            ps.setString(8, item.getPerfil());
            ps.setInt(9, item.getReferido());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public boolean delete(CrearClienteDTO item) {
        return false;
    }

    @Override
    public boolean update(CrearClienteDTO item) {
        PreparedStatement ps;
        try {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setInt(1, item.getReferido());
            ps.setLong(2, item.getDocumento());            
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            throw new Error(ex.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return false;
    }

    @Override
    public CrearClienteDTO read(Object key) {
        CrearClienteDTO objRes = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READ);
            psnt.setLong(1, Long.valueOf(key.toString()));
            ResultSet rs = psnt.executeQuery();
            while (rs.next()) {
                objRes = new CrearClienteDTO(rs.getLong("documento"),
                                    rs.getString("nombre"),
                                    rs.getString("direccion"),
                                    rs.getString("telefono"),
                                    rs.getString("ciudad"),
                                    rs.getString("usuario"),
                                    rs.getString("password"),
                                    rs.getString("perfil"),
                                    rs.getInt("referido"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return objRes;
    }

    @Override
    public List<CrearClienteDTO> readAll(CrearClienteDTO filter) {
        List<CrearClienteDTO> lst = null;
        PreparedStatement psnt;
        try {
            psnt = con.getCnn().prepareStatement(SQL_READALL);
            ResultSet rs = psnt.executeQuery();
            lst = new ArrayList<>();
            while (rs.next()) {
                CrearClienteDTO obj = new CrearClienteDTO(rs.getLong("documento"),
                                    rs.getString("nombre"),
                                    rs.getString("direccion"),
                                    rs.getString("telefono"),
                                    rs.getString("ciudad"),
                                    rs.getString("usuario"),
                                    rs.getString("password"),
                                    rs.getString("perfil"),
                                    rs.getInt("referido"));
                lst.add(obj);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            con.cerrarConexion();
        }
        return lst;
    }
    
}
