/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;

import Utils.Utiles;

/**
 *
 * @author heze-
 */
public class InicioSesionDTO {
    
    private Long documento;
    private String usuario,password,perfil;

    public InicioSesionDTO(Long documento, String usuario, String password,String perfil) {
        this.documento = documento;
        this.usuario = usuario;
        this.password = password;
        this.perfil = perfil;
    }

    public InicioSesionDTO() {
    }
   
    public Long getDocumento() {
        return documento;
    }

    public void setDocumento(Long documento) {
        this.documento = documento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    
}
