/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;

/**
 *
 * @author heze-
 */
public class MarcaDTO {
    
    private int id_marca;
    private String nombre;

    public MarcaDTO(int id_cargo, String nombre) {
        this.id_marca = id_cargo;
        this.nombre = nombre;
    }

    public MarcaDTO() {
    }

    public int getId_marca() {
        return id_marca;
    }

    public void setId_marca(int id_marca) {
        this.id_marca = id_marca;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
}
