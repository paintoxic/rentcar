/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;


/**
 *
 * @author heze-
 */
public class EmpleadoDTO {
    
    private int id_empleado,id_sucursal,id_cargo;
    private float comisiones;
    private Long id_persona;

    public EmpleadoDTO(int id_empleado, Long id_persona,int id_sucursal, int id_cargo, float comisiones) {
        this.id_empleado = id_empleado;
        this.id_sucursal = id_sucursal;
        this.id_cargo = id_cargo;
        this.comisiones = comisiones;
        this.id_persona = id_persona;
    }

    public EmpleadoDTO() {
    }

    public int getId_empleado() {
        return id_empleado;
    }

    public void setId_empleado(int id_empleado) {
        this.id_empleado = id_empleado;
    }

    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public int getId_cargo() {
        return id_cargo;
    }

    public void setId_cargo(int id_cargo) {
        this.id_cargo = id_cargo;
    }

    public float getComisiones() {
        return comisiones;
    }

    public void setComisiones(float comisiones) {
        this.comisiones = comisiones;
    }

    public Long getId_persona() {
        return id_persona;
    }

    public void setId_persona(Long id_persona) {
        this.id_persona = id_persona;
    }    
}
