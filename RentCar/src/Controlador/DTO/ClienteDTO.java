/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;

/**
 *
 * @author heze-
 */
public class ClienteDTO {
 
    private int id_cliente;
    private Long id_persona;
    private int id_ciudad,referido;

    public ClienteDTO(int id_cliente, Long id_persona, int id_ciudad, int referido) {
        this.id_cliente = id_cliente;
        this.id_persona = id_persona;
        this.id_ciudad = id_ciudad;
        this.referido = referido;
    }

    public ClienteDTO() {
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Long getId_persona() {
        return id_persona;
    }

    public void setId_persona(Long id_persona) {
        this.id_persona = id_persona;
    }

    public int getId_ciudad() {
        return id_ciudad;
    }

    public void setId_ciudad(int id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

    public int getReferido() {
        return referido;
    }

    public void setReferido(int referido) {
        this.referido = referido;
    }
    
    
    
}
