/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;

/**
 *
 * @author heze-
 */
public class AlquilerClienteDTO {
    
    private int id_alquiler;
    private String cliente;
    private Long doc_cliente;
    private String empleado;
    private Long doc_empleado;
    private Float precio_alquiler;

    public AlquilerClienteDTO(int id_alquiler, String cliente, Long doc_cliente, String empleado, Long doc_empleado, Float precio_alquiler) {
        this.id_alquiler = id_alquiler;
        this.cliente = cliente;
        this.doc_cliente = doc_cliente;
        this.empleado = empleado;
        this.doc_empleado = doc_empleado;
        this.precio_alquiler = precio_alquiler;
    }

    public AlquilerClienteDTO() {
    }

    public int getId_alquiler() {
        return id_alquiler;
    }

    public void setId_alquiler(int id_alquiler) {
        this.id_alquiler = id_alquiler;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Long getDoc_cliente() {
        return doc_cliente;
    }

    public void setDoc_cliente(Long doc_cliente) {
        this.doc_cliente = doc_cliente;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public Long getDoc_empleado() {
        return doc_empleado;
    }

    public void setDoc_empleado(Long doc_empleado) {
        this.doc_empleado = doc_empleado;
    }

    public Float getPrecio_alquiler() {
        return precio_alquiler;
    }

    public void setPrecio_alquiler(Float precio_alquiler) {
        this.precio_alquiler = precio_alquiler;
    }

    
    
    
}
