/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;

/**
 *
 * @author heze-
 */
public class AlquilerDTO {
     
    private int id_alquiler,id_cliente,id_empleado;
    private float precio_alquiler;

    public AlquilerDTO(int id_alquiler, int id_cliente, int id_empleado, float precio_alquiler) {
        this.id_alquiler = id_alquiler;
        this.id_cliente = id_cliente;
        this.id_empleado = id_empleado;
        this.precio_alquiler = precio_alquiler;
    }

    public AlquilerDTO() {
    }

    public int getId_alquiler() {
        return id_alquiler;
    }

    public void setId_alquiler(int id_alquiler) {
        this.id_alquiler = id_alquiler;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getId_empleado() {
        return id_empleado;
    }

    public void setId_empleado(int id_empleado) {
        this.id_empleado = id_empleado;
    }

    public float getPrecio_alquiler() {
        return precio_alquiler;
    }

    public void setPrecio_alquiler(float precio_alquiler) {
        this.precio_alquiler = precio_alquiler;
    }
    
    
    
    
}
