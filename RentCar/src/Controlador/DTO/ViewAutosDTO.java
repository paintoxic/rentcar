/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;

/**
 *
 * @author heze-
 */
public class ViewAutosDTO {
    
    private String placa;
    private int modelo;
    private String estado;
    private float precio_dia;
    private String marca,color,sucursal;

    public ViewAutosDTO(String placa, int modelo, String estado, float precio_dia, String marca, String color, String sucursal) {
        this.placa = placa;
        this.modelo = modelo;
        this.estado = estado;
        this.precio_dia = precio_dia;
        this.marca = marca;
        this.color = color;
        this.sucursal = sucursal;
    }

    public ViewAutosDTO() {
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getModelo() {
        return modelo;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public float getPrecio_dia() {
        return precio_dia;
    }

    public void setPrecio_dia(float precio_dia) {
        this.precio_dia = precio_dia;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }
    
    
    
}
