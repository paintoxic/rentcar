/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;

/**
 *
 * @author heze-
 */
public class ColorDTO {
    
    private int id_color;
    private String nombre;

    public ColorDTO(int id_cargo, String nombre) {
        this.id_color = id_cargo;
        this.nombre = nombre;
    }

    public ColorDTO() {
    }

    public int getId_color() {
        return id_color;
    }

    public void setId_color(int id_color) {
        this.id_color = id_color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
}
