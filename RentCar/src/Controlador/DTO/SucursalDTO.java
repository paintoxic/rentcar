/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;

/**
 *
 * @author heze-
 */
public class SucursalDTO {
    
    private int id_sucursal;
    private String nombreSucursal,direccion;
    private int id_ciudad;

    public SucursalDTO(int id_sucursal, String nombreSucursal, String direccion, int id_ciudad) {
        this.id_sucursal = id_sucursal;
        this.nombreSucursal = nombreSucursal;
        this.direccion = direccion;
        this.id_ciudad = id_ciudad;
    }

    public SucursalDTO() {
    }
    
    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getId_ciudad() {
        return id_ciudad;
    }

    public void setId_ciudad(int id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

}
