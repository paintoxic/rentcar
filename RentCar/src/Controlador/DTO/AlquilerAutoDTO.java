/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.DTO;

import java.sql.Date;
/**
 *
 * @author heze-
 */
public class AlquilerAutoDTO {
    
    private int id_alquiler_auto,id_alquiler;
    private String placa;
    private Date fechaInicio,fechaFin;
    private int galonesentregado;
    private float precio_alquiler_auto;

    public AlquilerAutoDTO(int id_alquiler_auto, int id_alquiler, String placa, Date fechaInicio, Date fechaFin, int galonesentregado, float precio_alquiler_auto) {
        this.id_alquiler_auto = id_alquiler_auto;
        this.id_alquiler = id_alquiler;
        this.placa = placa;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.galonesentregado = galonesentregado;
        this.precio_alquiler_auto = precio_alquiler_auto;
    }

    public AlquilerAutoDTO() {
    }

    public int getId_alquiler_auto() {
        return id_alquiler_auto;
    }

    public void setId_alquiler_auto(int id_alquiler_auto) {
        this.id_alquiler_auto = id_alquiler_auto;
    }

    public int getId_alquiler() {
        return id_alquiler;
    }

    public void setId_alquiler(int id_alquiler) {
        this.id_alquiler = id_alquiler;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getGalonesentregado() {
        return galonesentregado;
    }

    public void setGalonesentregado(int galonesentregado) {
        this.galonesentregado = galonesentregado;
    }

    public float getPrecio_alquiler_auto() {
        return precio_alquiler_auto;
    }

    public void setPrecio_alquiler_auto(float precio_alquiler_auto) {
        this.precio_alquiler_auto = precio_alquiler_auto;
    }

    

}
